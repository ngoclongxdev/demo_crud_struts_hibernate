package model;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import util.HibernateUtil;
import entities.Book;

/**
 * @author Phuong Do
 */
public class BookModel {

    // 1. Get all books.
    public List<Book> getBooks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Book");
            List<Book> lstBooks = query.list();
            transaction.commit();
            return lstBooks;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return null;
    }

    // 2. Add new book.
    public boolean addNew(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
            return true;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return false;
    }

    // 3. Get book by id.
    public Book getBookByID(int bookID) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Book book = (Book) session.get(Book.class, bookID);
            transaction.commit();
            return book;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return null;
    }

    // 4. Update book.
    public boolean update(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(book);
            transaction.commit();
            return true;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return false;
    }

    // 5. Delete book.
    public boolean delete(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(book);
            transaction.commit();
            return true;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return false;
    }

    // 6. Search book by book name.
    public List<Book> searchBookByName(String bookName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session
                    .createQuery("FROM Book WHERE BookName like :bookName");
            query.setString("bookName", "%" + bookName + "%");
            List<Book> lstBooks = query.list();
            transaction.commit();
            return lstBooks;
        } catch (Exception e) {
            if (!(transaction == null)) {
                transaction.rollback();
            }
        } finally {
            session.close();
        }
        return null;
    }
}